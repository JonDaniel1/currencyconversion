package com.synergysuite.msa.currencyconversion;

import com.synergysuite.msa.currencyconversion.domain.CompanyCurrencyExchangeRate;
import com.synergysuite.msa.currencyconversion.service.local.CurrencyConversionService;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@RunWith(SpringRunner.class)
@AutoConfigureTestDatabase(replace=AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles({"test"})
// The main problem with the H2 database used in a test like this, is it drops it immediately afterwards and sets it up with a random name, making it very hard to query - it's fine for automated
// tests, just not development.
// To query in the in memory database, add a Thread.sleep(large number) and access http://localhost:9090/currency/h2-console/ for access to the local database, as defined in
// test resources application.properties.
// You can also query from code using the autowired jdbcTemplate object in this class.
@Slf4j
public class CurrencyServiceTest {
    @LocalServerPort
    protected int port = 9090;

    @Autowired
    protected CurrencyConversionService currencyConversionService;

    @Test
    public void testGet(){
        List<CompanyCurrencyExchangeRate> currencyRateList = currencyConversionService.getRatesForCompany(20000L);
        Assert.assertTrue( currencyRateList.size() > 0);
        System.out.println(currencyRateList.size());
    }

    @Test
    public void testSynchronise() throws Exception {
        currencyConversionService.synchroniseCurrencies("USD");
    }

    @Test
    public void testSynchroniseAll() throws Exception {
        currencyConversionService.synchroniseAll();
    }

    @Test
    public void testGetForCompany() {
        CompanyCurrencyExchangeRate companyCurrencyExchangeRate = currencyConversionService.getRateForCompany(20000L, "EUR", "USD", new java.util.Date());
        System.out.println(companyCurrencyExchangeRate.getExchange_rate());
    }

    @Test
    public void testGetLatestForCompanySameCurrency() {
        CompanyCurrencyExchangeRate companyCurrencyExchangeRate = currencyConversionService.getLatestRateForCompany(21000L);
        Assert.assertEquals((Double) companyCurrencyExchangeRate.getExchange_rate(), (Double) 1.0d);
    }

    @Test
    public void testGetLatestForCompanyDifferentCurrency() {
        CompanyCurrencyExchangeRate companyCurrencyExchangeRate = currencyConversionService.getLatestRateForCompany(22000L);
        System.out.println("From " + companyCurrencyExchangeRate.getFromCurrencyId().getAbbreviation() + " -> " + companyCurrencyExchangeRate.getToCurrencyId().getAbbreviation() + " : " + companyCurrencyExchangeRate.getExchange_rate());
        Assert.assertNotEquals((Double) companyCurrencyExchangeRate.getExchange_rate(), (Double) 1.0d);
    }

    @Test
    public void testCopyToCorporate() {
        currencyConversionService.copyRatesToCorporateNow(20000L);
    }

}
