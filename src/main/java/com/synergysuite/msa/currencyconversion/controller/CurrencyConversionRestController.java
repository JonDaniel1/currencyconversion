package com.synergysuite.msa.currencyconversion.controller;

import com.synergysuite.exceptions.ServiceException;
import com.synergysuite.msa.currencyconversion.domain.CompanyCurrencyExchangeRate;
import com.synergysuite.msa.currencyconversion.service.local.CurrencyConversionService;
import io.swagger.annotations.Api;
import java.util.List;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/currency/api/v1")
@CrossOrigin("*")
@Slf4j
@Api(value="Currency Conversions Controller")
public class CurrencyConversionRestController {

    @Autowired
    private CurrencyConversionService service;

    @PutMapping("/synchronise/{currencycode}")
    public ResponseEntity synchroniseCurrencyConversions(@Valid @PathVariable("currencycode") String currencyCode) throws ServiceException {
        return ResponseEntity.ok(service.synchroniseCurrencies(currencyCode));
    }

    @PutMapping("/synchroniseAll")
    public ResponseEntity synchroniseAll() throws ServiceException {
        return ResponseEntity.ok(service.synchroniseAll());
    }

    @PutMapping("/synchronise/corporate/{corporateId}")
    public ResponseEntity synchroniseForCorporate(@Valid @PathVariable("corporateId") Long corporateId) throws ServiceException {
        return ResponseEntity.ok(service.copyRatesToCorporate(corporateId));
    }

    @GetMapping("/get/{companyid}")
    public ResponseEntity getRates(@Valid @PathVariable("companyid") Long companyId){
        List<CompanyCurrencyExchangeRate> response = service.getRatesForCompany(companyId);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/get/latest/{companyid}")
    public ResponseEntity getLatestRateForCompany(@Valid @PathVariable("companyid") Long companyId) throws ServiceException {
        return ResponseEntity.ok(service.getLatestRateForCompany(companyId));
    }

    @GetMapping("/get/{companyid}/{fromcurrency}/{tocurrency}/{date}")
    public ResponseEntity getRate(@Valid @PathVariable("companyid") Long companyId, @Valid @PathVariable("fromcurrency") String fromCurrency,
                                  @Valid @PathVariable("tocurrency") String toCurrency, @Valid @PathVariable("date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) java.util.Date date ){
        CompanyCurrencyExchangeRate response = service.getRateForCompany( companyId, fromCurrency, toCurrency, date);
        return ResponseEntity.ok(response);
    }

}
