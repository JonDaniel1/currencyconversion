package com.synergysuite.msa.currencyconversion.business;

import com.synergysuite.utility.SynergyIDGenerator;
import java.io.Serializable;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;

public class BigIntegerSequenceGenerator implements IdentifierGenerator {
    @Override
    public Serializable generate(SharedSessionContractImplementor sharedSessionContractImplementor, Object o) throws HibernateException {
        return SynergyIDGenerator.getNewId();
    }
}
