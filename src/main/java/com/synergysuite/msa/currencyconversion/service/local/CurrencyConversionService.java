package com.synergysuite.msa.currencyconversion.service.local;

import com.synergysuite.exceptions.ServiceException;
import com.synergysuite.msa.currencyconversion.client.FixerIOResponse;
import com.synergysuite.msa.currencyconversion.domain.Company;
import com.synergysuite.msa.currencyconversion.domain.CompanyCurrencyExchangeRate;
import com.synergysuite.msa.currencyconversion.domain.Currency;
import com.synergysuite.msa.currencyconversion.repository.CompanyCurrencyExchangeRateRepository;
import com.synergysuite.msa.currencyconversion.repository.CurrencyRepository;
import com.synergysuite.msa.currencyconversion.service.remote.CompanyService;
import com.synergysuite.msa.dto.utility.ValueDisplayReference;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Example;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

@Service
@Slf4j
public class CurrencyConversionService implements Serializable {

    @Autowired
    private CompanyService companyService;

    @Autowired
    private CompanyCurrencyExchangeRateRepository exchangeRateRepository;

    @Autowired
    private CurrencyRepository currencyRepository;

    @Autowired
    private RestTemplate restTemplate;

    @Value("${synergysuite.setting-service.url:http://microservices.synergysuite.io:8555}")
    private String settingServiceBaseURL;

    @PersistenceContext
    private EntityManager entityManager;

    private static String ENDPOINT = "https://api.exchangeratesapi.io/latest"; // http://data.fixer.io/api/latest";
    // Lots of alternatives to this with roughly the same API style.
    private static String ACCESS_KEY = "b9384f97de7a4bb41afb015a4a477a7f";

    private static String COMPANY_SETTING_SYNCHRONISE_RULES = "CURRENCY_SYNC_RULES";

    public String synchroniseCurrencies(String baseCurrencyCode) throws ServiceException {
        Invocation.Builder apiRequest =
                ClientBuilder
                        .newClient()
                        .target(ENDPOINT)
                        .queryParam("access_key", ACCESS_KEY)
                        .queryParam("base", baseCurrencyCode)
                        .request();

        Response apiResponse = apiRequest.get();

        if (Response.Status.OK.getStatusCode() == apiResponse.getStatus()) {
            // Extract data
            FixerIOResponse fixerIOResponse = apiResponse.readEntity(FixerIOResponse.class);
            // save data
            Map<String,Currency> currencies = currencyRepository.findAll().stream().collect(Collectors.toMap(Currency::getAbbreviation, Function.identity()));
            Currency baseCurrency = currencies.get(fixerIOResponse.getBase());
            if(baseCurrency == null){
                throw new ServiceException("Base currency " + fixerIOResponse.getBase() + " is not recognised.");
            }

            for(String iso4217Code : fixerIOResponse.getRates().keySet()){
                Currency currentCurrency = currencies.get(iso4217Code);
                if(currentCurrency == null) { continue; }

                // check if it's already there first - if it is, we purge it and recreate as the rate may have been updated.
                Optional<CompanyCurrencyExchangeRate> existing = exchangeRateRepository.findOne( Example.of( CompanyCurrencyExchangeRate.builder().companyId(0L)
                        .corporateId(0L)
                        .fromCurrencyId(baseCurrency)
                        .toCurrencyId(currentCurrency)
                        .fromDate( java.sql.Date.valueOf(fixerIOResponse.getDate())).build()));

                if(existing.isPresent()){
                    exchangeRateRepository.delete(existing.get());
                }

                CompanyCurrencyExchangeRate rate = CompanyCurrencyExchangeRate.builder()
                        .companyId(0L)
                        .corporateId(0L)
                        .fromCurrencyId(baseCurrency)
                        .toCurrencyId(currentCurrency)
                        .fromDate( java.sql.Date.valueOf(fixerIOResponse.getDate()))
                        .exchange_rate( fixerIOResponse.getRates().get(iso4217Code) )
                        .build();

                exchangeRateRepository.save(rate);
            }
        } else {
            throw new ServiceException("Remote API endpoint " + ENDPOINT + " returned status code " + apiResponse.getStatus());
        }
        return(null);
    }

    public String synchroniseAll() throws ServiceException {
        Query query = entityManager.createNativeQuery(" select distinct currency.abbreviation " +
                        "  from company inner join currency on (company.base_currency_id = currency.id ) " +
                        " where company_type_id in (0) ");

        List<String> result = query.getResultList();
        for(String abbreviation : result){
            synchroniseCurrencies(abbreviation);
        }


        // Need to check with Tim if there's a way to identify which corporates are active.
        Query query2 = entityManager.createNativeQuery(" select id from corporate ");

        List<BigInteger> corporates = query2.getResultList();

        corporates.stream().forEach( corporateId -> copyRatesToCorporate(corporateId.longValue()));

        return(null);
    }

    public String copyRatesToCorporate(Long corporateId){
        // need new setting for this.  Semi-colon separated thing.  Expecting DAILY/WEEKLY/MONTHLY ; int (where int = day of week / day of month )
        // e.g. WEEKLY;1 MONTHLY;15
        String companyCurrencyRules = getSetting(corporateId, COMPANY_SETTING_SYNCHRONISE_RULES);
        log.debug(companyCurrencyRules);
        if(companyCurrencyRules == null || companyCurrencyRules.isEmpty()){
            // Default is to synchronise daily.
            companyCurrencyRules = "DAILY;1";
        }
        String[] splitParameters = companyCurrencyRules.split(";");
        String frequency = splitParameters[0];
        Integer when = null;
        if(frequency.equals("WEEKLY") || frequency.equals("MONTHLY")){
            if(splitParameters.length < 2){ throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Setting " + COMPANY_SETTING_SYNCHRONISE_RULES + " has an incorrect setting."); }
            try {
                when = Integer.parseInt(splitParameters[1]);
            } catch (NumberFormatException e){
                throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Setting " + COMPANY_SETTING_SYNCHRONISE_RULES + " has an incorrect setting.");
            }
        }
        if(frequency.equals("DAILY")){
            copyRatesToCorporateNow(corporateId);
            return(null);
        }
        Calendar cal = Calendar.getInstance();
        if(frequency.equals("WEEKLY")){
            if(cal.get(Calendar.DAY_OF_WEEK) == when){
                copyRatesToCorporateNow(corporateId);
            }
            return(null);
        }
        if(frequency.equals("MONTHLY")){
            if(cal.get(Calendar.DAY_OF_MONTH) == when){
                copyRatesToCorporateNow(corporateId);
            }
            return(null);
        }
        return(null);
    }

    public String copyRatesToCorporateNow(Long corporateId){
        String iso4217Code = getHeadOfficeCurrencyUsingCorporateId(corporateId);
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_YEAR, -1);
        List<CompanyCurrencyExchangeRate> exchangeRates =
                exchangeRateRepository.findByCorporateIdAndFromDate(0L, cal.getTime()).orElseThrow( () -> new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "No rates " +
                        "were found to copy"));

        for(CompanyCurrencyExchangeRate exchangeRate : exchangeRates){
            CompanyCurrencyExchangeRate newCompanyRate =
                                    CompanyCurrencyExchangeRate.builder().exchange_rate( exchangeRate.getExchange_rate())
                                    .fromCurrencyId(exchangeRate.getFromCurrencyId())
                                    .toCurrencyId(exchangeRate.getToCurrencyId())
                                    .fromDate( exchangeRate.getFromDate())
                                    .toCurrencyId( exchangeRate.getToCurrencyId())
                                    .corporateId( corporateId )
                                    .build();

            Optional<CompanyCurrencyExchangeRate> existing = exchangeRateRepository.findOne( Example.of( CompanyCurrencyExchangeRate.builder()
                    .corporateId(newCompanyRate.getCorporateId())
                    .fromCurrencyId(newCompanyRate.getFromCurrencyId())
                    .toCurrencyId(newCompanyRate.getToCurrencyId())
                    .fromDate( newCompanyRate.getFromDate()).build()));

            if(existing.isPresent()){
                exchangeRateRepository.delete(existing.get());
            }

            exchangeRateRepository.save(newCompanyRate);
        }
        return(null);
    }

    public List<CompanyCurrencyExchangeRate> getRatesForCompany(Long companyId){
        List<CompanyCurrencyExchangeRate> exchangeRates = exchangeRateRepository.findByCorporateId(companyId).orElseThrow( () -> new ResponseStatusException(HttpStatus.NO_CONTENT,
                "No exchange rates were found for company " + companyId));
        return(exchangeRates);
    }

    public CompanyCurrencyExchangeRate getRateForCompany(Long companyId, String fromCurrencyCode, String toCurrencyCode, java.util.Date date){
        if(fromCurrencyCode.equals(toCurrencyCode)){ return(sameCurrency(companyId)); }
        List<CompanyCurrencyExchangeRate> exchangeRates =
                exchangeRateRepository.findLatest(companyId, date, fromCurrencyCode, toCurrencyCode ).orElse( exchangeRates = null );

        if(exchangeRates == null || exchangeRates.isEmpty()){
            // Try the other way around.
            exchangeRates =
                    exchangeRateRepository.findLatest(companyId, date, toCurrencyCode, fromCurrencyCode ).orElseThrow( () -> new ResponseStatusException(HttpStatus.NO_CONTENT,
                            "No exchange rates were found for company " + companyId));
                    // need to swap them around if it's like this.
                    Currency from = exchangeRates.get(0).getFromCurrencyId();
                    Currency to = exchangeRates.get(0).getToCurrencyId();
                    exchangeRates.get(0).setExchange_rate( 1.0d / exchangeRates.get(0).getExchange_rate());
                    exchangeRates.get(0).setFromCurrencyId(to);
                    exchangeRates.get(0).setToCurrencyId(from);
        }

        return(exchangeRates.get(0));
    }

    public String getHeadOfficeCurrencyUsingCorporateId(Long corporateId){
        Query query = entityManager.createNativeQuery(" select distinct currency.abbreviation " +
                "  from company headoffice " +
                "    inner join currency on ( headoffice.Base_Currency_ID = currency.id ) " +
                " where headoffice.corporate_id = ?1 " +
                "   and headoffice.company_type_id = 0 ")
                .setParameter(1, corporateId);

        List<String> currencyCode = (List<String>) query.getResultList();
        if(currencyCode != null && currencyCode.size() > 0 ){
            return(currencyCode.get(0));
        } else {
            log.warn("Company Currency for company {} is not set", corporateId );
            return(null);
        }
    }

    public String getHeadOfficeCurrencyUsingCompanyId(Long companyId){
        Query query = entityManager.createNativeQuery(" select distinct currency.abbreviation " +
                "  from company outlet inner join company headoffice on ( outlet.corporate_id = headoffice.corporate_id and headoffice.company_type_id = 0 ) " +
                "    inner join currency on ( headoffice.Base_Currency_ID = currency.id ) " +
                " where outlet.id = ?1 ")
                .setParameter(1, companyId);

        List<String> currencyCode = (List<String>) query.getResultList();
        if(currencyCode != null && currencyCode.size() > 0 ){
            return(currencyCode.get(0));
        } else {
            log.warn("Company Currency for company {} is not set", companyId );
            return(null);
        }
    }

    public String getCompanyCurrency(Long companyId){
        Query query = entityManager.createNativeQuery(" select distinct currency.abbreviation " +
                "  from company inner join currency on (company.base_currency_id = currency.id ) " +
                " where company.id = ?1 ")
                .setParameter(1, companyId);

        List<String> currencyCode = (List<String>) query.getResultList();
        if(currencyCode != null  && currencyCode.size() > 0 ){
            return(currencyCode.get(0));
        } else {
            log.warn("Company Currency for company {} is not set", companyId );
            return(null);
        }
    }

    public CompanyCurrencyExchangeRate getLatestRateForCompany(Long companyId){
        // NB - doesn't work properly, microservice not returning me the currency id so a bit inefficient really.
        Optional<Company> company = companyService.findById(companyId);
        String fromCurrencyCode = getHeadOfficeCurrencyUsingCompanyId(companyId);
        String toCurrencyCode = getCompanyCurrency(companyId);
        if(fromCurrencyCode.equals(toCurrencyCode)){
            return(sameCurrency(companyId));
        }
        return( getLatestRateForCompany(company.get().getCorporate().getId(), fromCurrencyCode, toCurrencyCode));
    }

    private CompanyCurrencyExchangeRate sameCurrency(Long companyId){
        CompanyCurrencyExchangeRate sameCurrency = new CompanyCurrencyExchangeRate();
        sameCurrency.setCompanyId(companyId);
        sameCurrency.setCorporateId(companyId);
        sameCurrency.setExchange_rate(1.0d);
        sameCurrency.setFromCurrencyId(new Currency());
        sameCurrency.setToCurrencyId(new Currency());
        sameCurrency.setId(0L);
        return(sameCurrency);
    }

    public CompanyCurrencyExchangeRate getLatestRateForCompany(Long companyId, String fromCurrencyCode, String toCurrencyCode){
        if(fromCurrencyCode.equals(toCurrencyCode)){ return(sameCurrency(companyId)); }
        return( getRateForCompany(companyId, fromCurrencyCode, toCurrencyCode, new java.util.Date()));
    }

    private String getSetting(Long companyId, String key) {
        try {
            ValueDisplayReference response = restTemplate.getForObject(settingServiceBaseURL + "companySettings/v1.0/" + companyId + "/key/" + key, ValueDisplayReference.class);
            return response.getValue();
        } catch (Exception e) {
            log.warn("Unable to get setting {} for [{}]. Caused by {}", key, companyId, e.getMessage());
            return null;
        }
    }

}
