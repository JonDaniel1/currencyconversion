package com.synergysuite.msa.currencyconversion.service.remote;

import com.synergysuite.msa.currencyconversion.config.FeignConfig;
import com.synergysuite.msa.currencyconversion.domain.Company;
import com.synergysuite.msa.currencyconversion.service.remote.fallback.CompanyServiceFallback;
import java.util.Optional;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Primary;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RibbonClient(name = "company-service")
@FeignClient(name = "company-service", fallback = CompanyServiceFallback.class, configuration = FeignConfig.class)
@Primary
public interface CompanyService {
    @RequestMapping(value = "/companies/v1.0/{id}", method = GET)
    Optional<Company> findById(@PathVariable("id") Long companyId);
}