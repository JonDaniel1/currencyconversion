package com.synergysuite.msa.currencyconversion.service.remote.fallback;

import com.synergysuite.msa.currencyconversion.domain.Company;
import com.synergysuite.msa.currencyconversion.service.remote.CompanyService;
import java.util.Optional;
import org.springframework.stereotype.Component;

@Component
public class CompanyServiceFallback implements CompanyService {
    @Override
    public Optional<Company> findById(Long companyId) {
        throw new RuntimeException("CompanyServiceFallback error...");
    }
}