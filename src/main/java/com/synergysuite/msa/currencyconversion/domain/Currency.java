package com.synergysuite.msa.currencyconversion.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="currency")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Currency {
    @Id
    @GenericGenerator(name = "SEQUENCE_BIGINT_ID", strategy = "com.synergysuite.msa.currencyconversion.business.BigIntegerSequenceGenerator")
    @GeneratedValue(generator = "SEQUENCE_BIGINT_ID")
    @Column(name="id")
    long id;
    @Column(name="name")
    String name;
    @Column(name="symbol")
    String symbol;
    @Column(name="abbreviation")
    String abbreviation;
    @Column(name="euro_linked")
    short euroLinked;
    @Column(name="euro_linked_value")
    Double euroLinkedValue;
    @Column(name="realex_code")
    String realex_code;
    @Column(name="servebase_code")
    String servebaseCode;
}
