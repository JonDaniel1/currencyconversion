package com.synergysuite.msa.currencyconversion.domain;

import com.synergysuite.msa.dto.corporate.Corporate;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Company {

    private Long id;

    private Long baseCurrencyId;

    private Corporate corporate;

}
