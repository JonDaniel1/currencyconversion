package com.synergysuite.msa.currencyconversion.domain;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

@Entity
@Table(name="company_currency_exchange_rate")
@NamedQuery(name = "CompanyCurrencyExchangeRate.findLatest", query = " select c from CompanyCurrencyExchangeRate c where c.corporateId = ?1 " +
        " and fromDate <= ?2 and fromCurrencyId.abbreviation = ?3 and toCurrencyId.abbreviation = ?4  order by fromDate desc " )

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CompanyCurrencyExchangeRate {
    @Id
    @GenericGenerator(name = "SEQUENCE_BIGINT_ID", strategy = "com.synergysuite.msa.currencyconversion.business.BigIntegerSequenceGenerator")
    @GeneratedValue(generator = "SEQUENCE_BIGINT_ID")
    @Column(name="id")
    long id;
    @Column(name="corporate_id")
    long corporateId;
    @Column(name="company_id")
    long companyId;

    @JoinColumn(name="from_currency_id")
    @NotFound(action= NotFoundAction.IGNORE)
    @ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.DETACH)
    Currency fromCurrencyId;

    @JoinColumn(name="to_currency_id")
    @ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.DETACH)
    @NotFound(action= NotFoundAction.IGNORE)
    Currency toCurrencyId;

    @Column(name="from_date")
    java.sql.Date fromDate;
    @Column(name="exchange_rate")
    Double exchange_rate;

}
