package com.synergysuite.msa.currencyconversion.queue;

import com.synergysuite.exceptions.ServiceException;
import com.synergysuite.scheduler.ScheduledTaskRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.aws.messaging.core.QueueMessagingTemplate;
import org.springframework.stereotype.Service;

@Service
public class QueueService {
    public static final String TASK_COMPLETE = "scheduledTask_complete";

    @Autowired
    QueueMessagingTemplate queueMessagingTemplate;
    @Value("${synergy.aws.taskComplete}")
    private String taskCompleteQueue;

    public void sendTaskCompleted(ScheduledTaskRequest completedRequest) throws ServiceException {
        queueMessagingTemplate.convertAndSend(taskCompleteQueue, completedRequest);
    }
}
