package com.synergysuite.msa.currencyconversion.queue;

import com.synergysuite.exceptions.ServiceException;
import com.synergysuite.msa.currencyconversion.service.local.CurrencyConversionService;
import com.synergysuite.scheduler.ScheduledTaskRequest;
import com.synergysuite.scheduler.ScheduledTaskTypeMS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.aws.messaging.listener.annotation.SqsListener;
import org.springframework.stereotype.Service;

@Service
public class MainQueueListener {
    public final String queueName;
    private final CurrencyConversionService currencyService;
    private final QueueService queueService;

    @Autowired
    public MainQueueListener(@Value("${synergy.aws.listener.main.queue:@null}") String queueName, CurrencyConversionService service, QueueService queueService) {
        this.queueName = queueName;
        this.currencyService = service;
        this.queueService = queueService;
    }

    @SqsListener(value = "${synergy.aws.listener.main.queue}")
    public void onMessage(ScheduledTaskRequest request) throws ServiceException {
        // TODO - not DUMMY
        if (!ScheduledTaskTypeMS.DUMMY.name().equals(request.getTaskType()))
            throw new ServiceException("This service is not registered for that task type. Aborting message processing...");
        currencyService.synchroniseAll();
        queueService.sendTaskCompleted(request);
    }
}
