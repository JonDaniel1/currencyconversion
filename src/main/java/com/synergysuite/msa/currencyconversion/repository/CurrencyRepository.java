package com.synergysuite.msa.currencyconversion.repository;

import com.synergysuite.msa.currencyconversion.domain.CompanyCurrencyExchangeRate;
import com.synergysuite.msa.currencyconversion.domain.Currency;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CurrencyRepository extends JpaRepository<Currency,Long>,
        PagingAndSortingRepository<Currency, Long>,
        JpaSpecificationExecutor<Currency> {

}
