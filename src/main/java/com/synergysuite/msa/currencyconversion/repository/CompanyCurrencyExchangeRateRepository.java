package com.synergysuite.msa.currencyconversion.repository;

import com.synergysuite.msa.currencyconversion.domain.CompanyCurrencyExchangeRate;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompanyCurrencyExchangeRateRepository extends JpaRepository<CompanyCurrencyExchangeRate,Long>,
        PagingAndSortingRepository<CompanyCurrencyExchangeRate, Long>,
        JpaSpecificationExecutor<CompanyCurrencyExchangeRate> {

    Optional<List<CompanyCurrencyExchangeRate>> findByCompanyId(Long companyId);

    Optional<List<CompanyCurrencyExchangeRate>> findByCorporateId(Long corporateId);

    Optional<List<CompanyCurrencyExchangeRate>> findByCorporateIdAndFromDate(Long corporateId, Date date);

    Optional<List<CompanyCurrencyExchangeRate>> findLatest(Long corporateId, Date date, String fromCurrencyCode, String toCurrencyCode);

}
