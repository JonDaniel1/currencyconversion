###
#   Server Settings
###
server.servlet.context-path=/currency
server.port=8080
spring.data.rest.base-path=/api

###
#   Spring Boot Admin Settings
#	http://codecentric.github.io/spring-boot-admin/2.1.3/
###
spring.boot.admin.client.url=http://j101.synergysuite.net:8019
spring.boot.admin.client.instance.service-url=http://j101.synergysuite.net:8023${server.servlet.context-path}
spring.boot.admin.client.auto-deregistration=true

###
#   Eureka Settings
#	https://cloud.spring.io/spring-cloud-static/Greenwich.RC2/multi/multi__service_discovery_eureka_clients.html
###
eureka.client.register-with-eureka=true

eureka.instance.hostName=currency

# preferIpAddress, IP addresses of services rather than the hostname.
eureka.instance.preferIpAddress=true

# statusPageUrlPath, The status page and health indicators for a Eureka instance default to "/info".
eureka.instance.statusPageUrlPath=/currency/actuator/info

# statusPageUrlPath, The status page and health indicators for a Eureka instance default to "/health.
eureka.instance.healthCheckUrlPath=/currency/actuator/health

# instance heartbeats interval. Default 30s.
eureka.instance.leaseRenewalIntervalInSeconds=30

# If the server doesn't receive a heartbeat. Default 90s
eureka.instance.leaseExpirationDurationInSeconds=90

# defaultZone, the service URL. server1, server2, server3 (fallback).
eureka.client.service-url.defaultZone=http://microservices.synergysuite.io:8888/eureka

# healthcheck.enabled, determine if a client is up.
eureka.client.healthcheck.enabled=true

###
#   Ribbon Settings
#	https://github.com/Netflix/ribbon
# 	https://github.com/Netflix/ribbon/wiki/Getting-Started
###

### settings ###
# Explicitly enables the use of Eureka in Ribbon on another hand you need to use service.ribbon.listOfServers
company-settings.ribbon.eureka.enabled=false

company-settings.ribbon.listOfServers=microservices.synergysuite.io:8555

company-settings.ribbon.NIWSServerListClassName=com.netflix.loadbalancer.ConfigurationBasedServerList

# Interval to refresh the server list from the source
company-settings.ribbon.ServerListRefreshInterval=1000

# Max number of retries on the same server (excluding the first try)
company-settings.ribbon.MaxAutoRetries=3

# Max number of next servers to retry (excluding the first server)
company-settings.ribbon.MaxAutoRetriesNextServer=3

# Whether all operations can be retried for this client
company-settings.ribbon.OkToRetryOnAllOperations=true

company-settings.ribbon.ReadTimeout=20000


### company ###
# Explicitly enables the use of Eureka in Ribbon on another hand you need to use service.ribbon.listOfServers
company-service.ribbon.eureka.enabled=false

#recipe-service.ribbon.listOfServers=localhost:8081
company-service.ribbon.listOfServers=microservices.synergysuite.io:8385

company-service.ribbon.NIWSServerListClassName=com.netflix.loadbalancer.ConfigurationBasedServerList

# Interval to refresh the server list from the source
company-service.ribbon.ServerListRefreshInterval=1000

# Max number of retries on the same server (excluding the first try)
company-service.ribbon.MaxAutoRetries=3

# Max number of next servers to retry (excluding the first server)
company-service.ribbon.MaxAutoRetriesNextServer=3

# Whether all operations can be retried for this client
company-service.ribbon.OkToRetryOnAllOperations=true

company-service.ribbon.ReadTimeout=20000

###
#   Feign Settings
#   https://cloud.spring.io/spring-cloud-netflix/multi/multi_spring-cloud-feign.html
###

# feign.hystrix.enabled, If Hystrix is on the classpath and feign.hystrix.enabled=true, Feign will wrap all methods with a circuit breaker.
feign.hystrix.enabled=true

###
#   Hystrix Settings
#   https://github.com/Netflix/Hystrix/wiki/Configuration
###

# circuitBreaker.enabled, This property determines whether a circuit breaker will be used to track health and to short-circuit requests if it trips. Default value is true.
hystrix.command.default.circuitBreaker.enabled=true

# requestVolumeThreshold, This property sets the minimum number of requests in a rolling window that will trip the circuit. Default value 20.
hystrix.command.default.circuitBreaker.requestVolumeThreshold=2

# sleepWindowInMilliseconds, This property sets the amount of time, after tripping the circuit, to reject requests before allowing attempts again to determine if the circuit should again be closed. Default value 5000.
hystrix.command.default.circuitBreaker.sleepWindowInMilliseconds=500

# errorThresholdPercentage, This property sets the error percentage at or above which the circuit should trip open and start short-circuiting requests to fallback logic. Default value is 50.
hystrix.command.default.circuitBreaker.errorThresholdPercentage=50

# This property, if true, forces the circuit breaker into an open (tripped) state in which it will reject all requests. Default value is false.
hystrix.command.default.circuitBreaker.forceOpen=false

# This property, if true, forces the circuit breaker into a closed state in which it will allow requests regardless of the error percentage.  Default value is false.
hystrix.command.default.circuitBreaker.forceClosed=false

# This property sets the time in milliseconds after which the caller will observe a timeout and walk away from the command execution. Default value is 1000.
hystrix.command.default.execution.isolation.thread.timeoutInMilliseconds=60000

###
#   Actuator Settings
### ********** PERMISSIVE CONFIG TEST ONLY
#management.endpoints.web.base-path=/actuator
management.endpoint.health.enabled=true 
management.endpoints.jmx.exposure.include=* 
management.endpoints.web.exposure.include=* 
#management.endpoints.web.base-path=/actuator
management.endpoints.web.cors.allowed-origins=*
management.endpoints.web.cors.allowed-methods=*
management.endpoint.health.show-details=always

###
#   Datasource Settings
###

spring.datasource.url=jdbc:mysql://localhost/kapura?jdbcCompliantTruncation=false
spring.datasource.username=root
spring.datasource.password=1hefalump!
spring.datasource.driverClassName=com.mysql.jdbc.Driver

#spring.datasource.url=jdbc:h2:mem:testdb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=TRUE;
#spring.datasource.username=sa
#spring.datasource.password=password
#spring.datasource.driverClassName=org.h2.Driver
#spring.datasource.continue-on-error=true

###
#   Hibernate Settings
###
spring.jpa.hibernate.ddl-auto=none
spring.jpa.properties.hibernate.show_sql=false
spring.jpa.properties.hibernate.use_sql_comments=false
spring.jpa.properties.hibernate.format_sql=false
spring.jpa.properties.hibernate.jdbc.batch_size=500
spring.h2.console.enabled=true
spring.h2.console.path=/h2-console
#spring.jpa.show-sql=true
#spring.jpa.properties.hibernate.format_sql=true
#logging.level.org.hibernate.SQL=DEBUG
#logging.level.org.hibernate.type.descriptor.sql.BasicBinder=TRACE



###
#   Jackson Settings
###
spring.jackson.date-format=yyyy-MM-dd HH:mm:ss
spring.jackson.time-zone=Europe/Dublin

###
#   Jackson Settings
###
#spring.jackson.date-format=yyyy-MM-dd
#spring.jackson.serialization.write-dates-as-timestamps=false
#spring.jackson.time-zone=EST

###
# AWS settings
###
cloud.aws.region.auto=false
cloud.aws.region.static=eu-west-1
cloud.aws.credentials.access-key=AKIAJXOBLVB46ZDHMTJQ
cloud.aws.credentials.secret-key=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
cloud.aws.credentials.instance-profile=false
cloud.aws.stack.auto=false

synergy.aws.listener.main.queue=weather_service
synergy.aws.taskComplete=scheduledTask_complete